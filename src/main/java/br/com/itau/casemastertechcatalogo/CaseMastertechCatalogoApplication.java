package br.com.itau.casemastertechcatalogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class CaseMastertechCatalogoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaseMastertechCatalogoApplication.class, args);
	}

}
