package br.com.itau.casemastertechcatalogo.controller;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.casemastertechcatalogo.models.Catalogo;
import br.com.itau.casemastertechcatalogo.services.CatalogoService;

@RestController
@RequestMapping("/")
public class CatalogoController {

	@Autowired
	CatalogoService catalogoService;
	
	Logger logger = LoggerFactory.getLogger(CatalogoController.class);
	
	@GetMapping
	public Iterable<Catalogo> listar(){
		return catalogoService.listar();
	}
	
	@GetMapping("/{nome}")
	public Optional<Catalogo> buscar(@PathVariable String nome) {
		logger.info("Request recebida!");
		Optional<Catalogo> catalogoOptional = catalogoService.buscar(nome);
		
//		if(catalogoOptional.isPresent()) {
			return catalogoOptional;
//		}
//		
//	//	return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public void inserirTermo(@RequestBody Catalogo catalogo) {
		catalogoService.inserirCatalogo(catalogo);
	}

	
}
