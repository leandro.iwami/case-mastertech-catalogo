package br.com.itau.casemastertechcatalogo.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.casemastertechcatalogo.models.Catalogo;

public interface CatalogoRepository extends CrudRepository<Catalogo, String> {

	Optional<Catalogo> findByNome(String nome);

}
