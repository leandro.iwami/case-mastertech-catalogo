package br.com.itau.casemastertechcatalogo.services;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.casemastertechcatalogo.models.Catalogo;
import br.com.itau.casemastertechcatalogo.repositories.CatalogoRepository;

@Service
public class CatalogoService {

	@Autowired
	CatalogoRepository catalogoRepository;

	public Iterable<Catalogo> listar() {
		return catalogoRepository.findAll();
	}

	public Optional<Catalogo> buscar(String nome) {
		return catalogoRepository.findByNome(nome);
	}
	
	public void inserirCatalogo(Catalogo catalogo) {
		catalogoRepository.save(catalogo);
	}

	@PostConstruct
	public void inicializarBase() {
		Catalogo catalogo1 = new Catalogo();
		catalogo1.setNome("Curso1");
		catalogo1.setPreco(1000.000);
		catalogo1.setDisponivel(true);		
		catalogoRepository.save(catalogo1);

		Catalogo catalogo2 = new Catalogo();
		catalogo2.setNome("Curso2");
		catalogo2.setPreco(2000.000);
		catalogo2.setDisponivel(false);
		catalogoRepository.save(catalogo2);

	}
}
